/**
 *
 * @description :: Server-side actions for myQuiz
 * This controller is used to receive the overall stats per quiz for a logged in user. This data is then parsed and then
 * sent to the view to be handled(overviewstats.ejs).
 */

module.exports =  async function displayQuiz (req,res) {
   var userID = req.me.id;
   var quizObject = [];
   var statsObject = [];
   var numberOfQuizes = 0;
   var quizIDs = [];
   var quizCorrect = [];
   var quizTime = [];
   var questionsAnswered = [];
   var questionsInQuiz = [];
   var user;

   var test = [];

   user = await User.find({
       where: {id: userID},
       select: ['fullName']
   });

   quizObject = await quiz.find(
       {
           where: {qm_id: userID},
           select: ['id']
       }
   );


   numberOfQuizes = quizObject.length;

    //initialise the quizTime array
    for(var i = 0; i < numberOfQuizes; i++){
        quizTime.push(0);
        questionsAnswered.push(0);
        questionsInQuiz.push(0);
    }

    //parse returned object and store in array
    for(var i = 0; i < numberOfQuizes; i++)
    {
        quizIDs.push(quizObject[i].id);
    }

    //Use these quiz codes to retrieve scores for all quiz codes from stats model
    for(var i = 0; i < numberOfQuizes; i++)
    {
       var temp = 0;
       var temp2 = 0;
       var temp3 = 0;

       temp3 = await questions.count({quiz_code: quizIDs[i]})
        questionsInQuiz[i] = temp3;

       temp = await stats.count({
           where : {
               quiz_id: quizIDs[i],
               is_correct: 1
           }
       });

       temp2 = await stats.count({quiz_id:quizIDs[i]});
       //checks if divisible by zero if so sets score to 0 otherwise percentage
       if(temp2 != 0){
           quizCorrect.push(temp/temp2*100);
       }
       else
           quizCorrect.push(0);

    }

    //Use the quizcodes to retrieve and total up how much time is spent on a quiz on average

    for (var i =0; i < numberOfQuizes; i++) {
        var tempcount = 0;

        tempcount = await  stats.count({quiz_id: quizIDs[i]});

        questionsAnswered[i] = tempcount;

        statsObject = await stats.find({
            where: {quiz_id: quizIDs[i]},
            select: ['time']

        });

        var tempval = 0;
        for(var j = 0; j < statsObject.length; j++)
        {
            tempval = statsObject[j];
            quizTime[i] += parseInt(tempval.time);
        }

    }

    for (var i = 0; i < numberOfQuizes; i++)
    {
        quizTime[i] = quizTime[i]/questionsAnswered[i];

        quizTime[i] = quizTime[i] * questionsInQuiz[i];
    }

    return res.view('pages/analytics/overviewstats', {user: user[0].fullName, quizScores: quizCorrect, quizTimes: quizTime, quizIDs : quizIDs});
};


