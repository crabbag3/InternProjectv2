/**
 * This is the server side controller for the quiz analytics. It takes a parameter in which it uses to query the
 * database and retrieve the relevant stats which is then parsed, bundled and sent to the analytics view.(analytic.ejs)
 *
 */

module.exports =  async function displayQuiz (req,res) {

    var timeTakenQuiz = [];
    var questionIds = [];
    var questionScores = [];
    var questionTimes = [];
    var longestTime = [];

    var selectedQuiz = req.param('quiz_code');
    var averageTimeTakenQuiz = 0;
    var numberOfTimesTaken = 0;
    var numberOfQuestionsInQuiz = 0;
    var questionsAnswered = 0;
    var averageScoreQuiz = 0;

    //Gets all stats from stats table for a certain quiz
    timeTakenQuiz = await  stats.find({
        where: {quiz_id: selectedQuiz},
        select: ['*']
    });

    questionIds = await questions.find({
        where: {quiz_code: selectedQuiz},
        select: ['id']
    });

    // data2 = await stats.find({
    //     where: {quiz_id: selectedQuiz},
    //     select: ['time']
    // });
//initialise scores per question array
    for(var i = 0; i < questionIds.length; i++)
    {
        questionScores.push(0);
        longestTime.push(0);
    }

    //Gets scores per question
    for (var i = 0; i < questionIds.length; i++)
    {
        for (var j = 0; j < timeTakenQuiz.length; j++){
            sails.log(questionIds[i].id + " " + timeTakenQuiz[j].question_id);
            if(questionIds[i].id === timeTakenQuiz[j].question_id && timeTakenQuiz[j].is_correct == 1)
            {
                questionScores[i]++;
            }
        }
    }

    //Gets the average time taken per quiz and average score
    for(var i = 0; i < timeTakenQuiz.length; i++)
    {
        averageTimeTakenQuiz += parseInt(timeTakenQuiz[i].time);
        if(timeTakenQuiz[i].is_correct == 1)
        {
            averageScoreQuiz++;
        }
    }

    //Gets average time per question
    for (var i = 0; i < questionIds.length; i++)
    {
        var questTime = 0;
        var tempLong = 0;
        for (var j = 0; j < timeTakenQuiz.length; j++)
        {
            if(questionIds[i].id === timeTakenQuiz[j].question_id)
            {
                questTime += parseInt(timeTakenQuiz[j].time);
                sails.log(questTime);
                if(longestTime[i] < timeTakenQuiz[j].time){
                    longestTime[i] = timeTakenQuiz[j].time;
                    sails.log(longestTime);
                }
            }
        }
        questionTimes.push(questTime);
    }

    questionsAnswered = await stats.count({quiz_id: selectedQuiz});
    numberOfQuestionsInQuiz = await questions.count({quiz_code: selectedQuiz});
    numberOfTimesTaken = questionsAnswered/numberOfQuestionsInQuiz;
    averageScoreQuiz = averageScoreQuiz/numberOfTimesTaken;
    sails.log("======================================================================");
    sails.log("Number of questions answered: " + questionsAnswered);
    sails.log("Number of attempts: " + numberOfTimesTaken);
    sails.log("Total time Taken: " +averageTimeTakenQuiz);
    sails.log("Average time per quiz: " + averageTimeTakenQuiz/numberOfTimesTaken);
    sails.log("Average score for the quiz: " + averageScoreQuiz + "____" + averageScoreQuiz/numberOfQuestionsInQuiz*100+"%");
    sails.log("======================================================================");
    sails.log(questionScores);

    var bundleData = [averageScoreQuiz,numberOfTimesTaken,averageTimeTakenQuiz/numberOfTimesTaken]
    //sails.log(timeTakenQuiz);
    sails.log(questionTimes);
    //returns to the view with the analytic view
    return res.view('pages/analytics/analytic', {quizCode: selectedQuiz,bundled: bundleData,sample: questionScores, attempts: numberOfTimesTaken, averageTimeQs : questionTimes, longestTimeQs : longestTime});
};


