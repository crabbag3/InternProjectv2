module.exports = {


    friendlyName: 'Insert Stats',


    description: 'This controller inserts the stats into the database',
    //inputs defined on views page are defined again in js for validation (type matches model)
    inputs : {
        _csrf: {
            type: "string"
        },

        quiz_id : {
            type : "string"
        },
        question_id : {
            type : "string"
        },

        selected_option : {
            type : "string"
        },

        is_correct : {
            type : "string"
        },

        time : {
            type : "string"
        }
    },


    exits: {
        redirect: {
            description: 'The requesting user is already logged in.',
            responseType: 'redirect'
        }
    },


    fn: async function (inputs, exits) {
      //logs user input in terminal


      var str = inputs.is_correct;
      var sta = inputs.question_id;
      var sel = inputs.selected_option;
      var tempTime = inputs.time;

      //Some null values are passed to the controller so using split it removes the extra , and then filters out any null
      var options = sel.split(",");
      var Questions = sta.split(",");
      var trueFalse = str.split(",");
      var times = tempTime.split(",");

      trueFalse = trueFalse.filter(Boolean);
      options = options.filter(Boolean);
      times = times.filter(Boolean);

      //checks if the string is true and sets it to a boolean true otherwaise sets it boolean false
      var isTrueSet = (trueFalse[1] == 'true');
      for (var i = 0; i < trueFalse.length; i++) {
        if (trueFalse[i] === '1') {
          trueFalse[i] = true;
        }
        else {
          trueFalse[i] = false;
        }
      }

      //creates rows int eh databse for the stats table

      for (var i = 0; i < trueFalse.length; i++) {
        stats.create({quiz_id: inputs.quiz_id, question_id: Questions[i], selected_option: options[i],
          is_correct: trueFalse[i], time: times[i]
        }).exec(function (err) {
          if (err) {
            console.log(err);
          }


        });

      }
      return exits.redirect('../');

    },
};
