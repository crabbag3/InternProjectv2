module.exports = {


    friendlyName: 'Create questions',


    description: 'This Page creates the questions for the quiz',

    exits: {

        success: {
            viewTemplatePath: 'pages/quiz/createquestions',
        },

        redirect: {
            description: 'The requesting user is already logged in.',
            responseType: 'redirect'
        }

    },


    fn: async function (inputs, exits) {

        if (!this.req.me) {
            throw {redirect: '/'};
        }

        return exits.success();

    }
};
