/*This page is updates the quiz title then redirects to add create questions allowing the user to add more questions to the current quiz*/

module.exports = {


    friendlyName: 'Update Quiz',


    description: 'This Page updates the quiz',
    //inputs defined on views page are defined again in js for validation (type matches model)
    inputs : {
        id: {
            type: "string"
        },
        quiz_name : {
            type : "string"
        },
        qm_id : {
            type : "string"
        }
    },


    exits: {

        redirect: {
            description: 'The requesting user is already logged in.',
            responseType: 'redirect'
        }

    },





    fn: async function (inputs, res) {

        sails.log(inputs.id);

        //id ref'd in ejs. q_n and q_i passed through with inputs as they're user defined.
        quiz.update({id : inputs.id})
            .set({quiz_name: inputs.quiz_name})
            .exec(function(err) {
                if (err) {
                    console.log(err);
                }
                //redirects to createquestions with the quiz_code in the url

                return res.redirect('/createquestions?id=' + inputs.id);


            });
    },



};
