/*
*   A Controller to redirect to the correct view
*   To bypass the login check go to policies.js and write:
*   'quiz/view-takequiz': true,
 */


module.exports = async function takeQuiz (req, res) {

    //Request parameter of Code, This will be used to get the quiz_code to return the correct quiz.
    var quizCode = req.param('code');
    var quizFound = await quiz.count({id: quizCode});


    if(quizFound < 1){
        return res.redirect("/");
    }
    else
    {
        var retrievedQ = await questions.find({quiz_code: quizCode}); //A Variable to store the retrieved questions


        var testArray;
        var test2 = [];
        var x = retrievedQ[0].id; //takes the q_id of our first question

        var y = retrievedQ.length *4; //number of questions in the quiz

        //Finds all options based off the quiz_code correctly, had to put a quizcode in for the options model
        var sample = await  options.find({
            where: {quiz_code: quizCode},
            limit: (y),
            sort: 'q_id ASC',
            select: ['*']
        });

    }



    //Removed as it was getting incorrect indexes if two users created questions at same time
    /*try {
        var x = retrievedQ[0].id; //takes the q_id of our first question
        sails.log("Retrieved first question" + x);
        var y = retrievedQ.length *4; //number of questions in the quiz

        while(t < retrievedQ.length)
        {
            var test = await options.find({q_id : retrievedQ[t].id});
            sails.log(t);
        sails.log("==============================================================================================");
        sails.log(test);
        test2.push(test[0].id);
        test2.push(test[1].id);
        test2.push(test[2].id);
        test2.push(test[3].id);
        sails.log("==============================================================================================");

        t++;
        }


        sails.log(x - 1);
        for (var i = 0; i < retrievedQ.length; i++) {
            sails.log("Question id     " + retrievedQ[i].id);
            var retrievedO = await options.find({
                where: {q_id: retrievedQ[i].id},
                limit: (y),
                sort: 'q_id ASC',
                select: ['*']
            });
            //sails.log("Retrieved option         " +retrievedO.id);


            //A Variable to store the possible answers
        }

        //sails.log("==============================================================================================");
        //sails.log(testArray);
    }
    catch (e) {
        console.log("Code not valid    " + e);
    } */


    /*PASSES back the view of takequiz and passes an array of User objects declared as users with the value of variable
    *retrievedQ, though it is only one result it must still be indexed when used in the EJS
    *If there is no results it will redirect to the home page otherwise it will take the quiz */
    if (retrievedQ == null)
    {

        return res.redirect('/');
    }
    else {
        if (sample == null)
        {

            return res.redirect('/');
        }
        else {
            return res.view('pages/quiz/takequiz', {quest: retrievedQ, possible: sample, qCode: quizCode});
        }
    }

};
