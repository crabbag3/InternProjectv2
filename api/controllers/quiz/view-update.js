module.exports =  async function displayQuiz (req,res) {

    //requests qm_id
    var id = req.me.id;
    var quizid = req.param('id');
    var vquestions = [];
    var voptions = [];
    var correct = [];


    //retrives all the quizzes based off the qm_id
    var quizname = await quiz.find({id : quizid});

    var quizQuestions = await questions.find({quiz_code: quizid});
    var quizOptions = await options.find({quiz_code: quizid});

    for(var i = 0; i < quizQuestions.length; i++)
    {
        vquestions.push(quizQuestions[i].question);
    }
    for(var i =0; i < quizOptions.length; i++){
        voptions.push(quizOptions[i].answer);
        correct.push(quizOptions[i].is_correct);
    }



    //returns to the view with the questions passed
    return res.view('pages/quiz/update', {name: quizname[0].quiz_name,questions : vquestions, options : voptions, correct : correct});
};


