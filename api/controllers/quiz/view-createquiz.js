module.exports = {


    friendlyName: 'Create quiz',


    description: 'This Page creates the quiz',

    exits: {

        success: {
            viewTemplatePath: 'pages/quiz/createquiz',
        },

        redirect: {
            description: 'The requesting user is already logged in.',
            responseType: 'redirect'
        }

    },


    fn: async function (inputs, exits) {

        if (!this.req.me) {
            throw {redirect: '/'};
        }

        return exits.success();

    }
};
