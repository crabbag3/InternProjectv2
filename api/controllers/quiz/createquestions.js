//gets the quiz_code from the url
module.exports = {


    friendlyName: 'Create questions',


    description: 'This Page creates the questions and options',
    //inputs defined on views page are defined again in js for validation (type matches model)
    inputs: {
        question: {
            type: "string"
        },

        id: {
            type: "string"
        },

        answer: {
            type: "string"
        },
        answer2: {
            type: "string"
        },
        answer3: {
            type: "string"
        },
        answer4: {
            type: "string"
        },

        option: {
            type: "string"
        },
        ans: {
            type: "string"

        },
        ans2: {
            type: "string"

        },
        ans3: {
            type: "string"

        },
        ans4: {
            type: "string"

        },
        save: {

            type: "string"
        }
    },


    exits: {

        redirect: {
            description: 'The requesting user is already logged in.',
            responseType: 'redirect'
        }

    },



    //function that creates questions and posted to db
    //at the moment questions are not linked to quiz *HELP*
    fn: async function (inputs, exits) {
        //logs user input in terminal
        //temporary random int gen. UUID should be used. substring to match length set in
        q_id = Math.round(Math.random()*10000);
        q_id = q_id.toString();
        if(inputs.question.indexOf("?") > -1)
        {
        }
        else {
            inputs.question += '?';
        }
        //id ref'd in ejs. q_n and q_i passed through with inputs as they're user defined.

        questions.create({question: inputs.question, id: q_id, quiz_code: id}).exec(function (err) {
            if (err) {
            }
            else
            //redrict to createquestions with quiz_code (but at the moment generates a new quizcode each time)
                return exits.redirect('/createquestions?id=' + id);

        });


        option_id = Math.round(Math.random()*10000);
        option_id = option_id.toString();

        //var ans = inputs.answer;
        // var answers =ans.split(",");
        //  answers= answers.filter(Boolean);


        var optionarray = [] ;
        optionarray[0]= inputs.answer;
        optionarray[1]= inputs.answer2;
        optionarray[2]= inputs.answer3;
        optionarray[3]= inputs.answer4;
        for(var i = 0; i < 4;i++){
            if(optionarray[i] == ''){

                optionarray[i] = ' ';
            }
        }
        var idArray = [];
        idArray[0]=option_id + 1;
        idArray[1]=option_id + 2;
        idArray[2]=option_id + 3;
        idArray[3]=option_id + 4;


        var optionPicked = [];

        if(inputs.ans == 'option_1') {
            optionPicked[0] = 1;
        }
        else{
            optionPicked[0] = 0;
            }if(inputs.ans2 == 'option_2'){

            optionPicked[1] =  1;

        }
        else{
            optionPicked[1] = 0;
        }
        if(inputs.ans3 == 'option_3') {

            optionPicked[2] = 1;

        }else{
            optionPicked[2]= 0
        }
         if(inputs.ans4 == 'option_4') {

            optionPicked[3] = 1;
        }
        else{
            optionPicked[3] = 0;
         }
        for(i = 0; i < optionarray.length;i++) {
            options.create({
                    q_id: q_id,
                    quiz_code: id,
                    answer: optionarray[i],
                    id: idArray[i],
                    is_correct:optionPicked[i]
                }
            ).exec(function (err) {
                if (err) {
                }
            });
        }
        if(inputs.save == '1') {
            return exits.redirect('/myQuiz');
        }

    },

};
