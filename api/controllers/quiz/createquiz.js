module.exports = {


    friendlyName: 'Create quiz',


    description: 'This Page creates the quiz',
 //inputs defined on views page are defined again in js for validation (type matches model)
    inputs : {
        quiz_name : {
            type : "string"
        },
        qm_id : {
            type : "string"
        }
    },


    exits: {

        redirect: {
            description: 'The requesting user is already logged in.',
            responseType: 'redirect'
        }

    },





    fn: async function (inputs, res) {
        //logs user input in terminal
        id = Math.round(Math.random()*10000);
        id = id.toString();
        //id = id.substr(0, 6);
        //temporary random int gen. UUID should be used. substring to match length set in model


        //id ref'd in ejs. q_n and q_i passed through with inputs as they're user defined.
        quiz.create({id : id, quiz_name: inputs.quiz_name, qm_id : inputs.qm_id}).exec(function(err) {
            if (err) {
            }
            //redirects to createquestions with the quiz_code in the url

            return res.redirect('/createquestions?id='+ id);


        });
    },




};
