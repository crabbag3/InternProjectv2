/**
 *
 * @description :: Server-side actions for myQuiz
 */

module.exports =  async function displayQuiz (req,res) {

    //requests qm_id
    var id = req.me.id;
    var timesTaken = [];


    //retrives all the quizzes based off the qm_id
    var retrievedQuiz = await quiz.find({qm_id: id}); //variable to store retrived qs

    sails.log(retrievedQuiz);
    var size = retrievedQuiz.length;
    for(var counter = 0; counter < size; counter++) {
        var count = await stats.count({quiz_id: retrievedQuiz[counter].id});
        var quests = await questions.count({quiz_code: retrievedQuiz[counter].id});
        count = count/quests;
        timesTaken.push(count);
    }


    if (retrievedQuiz == null)
    {
        return res.redirect('/');
    }
    else {
        //returns to the view with the questions passed
        return res.view('pages/myQuizes/myQuiz', {quiz: retrievedQuiz, count: timesTaken});
    }
};


