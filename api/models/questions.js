module.exports = {
  attributes: {
      id: {
          columnName: 'q_id',
          type : 'string',
          required : true,
          unique : true,
          example : '1234f'
      },

      question: {
          type : 'string',
          required : true,
          example : 'What is WIPs'
      },

      subject: {
          type : 'string',
          required : false,
          example : 'WIPs'
      },

      quiz_code: {
          type: 'string',
          required: true,
          example: '2ewaq2'
      }
  }

};