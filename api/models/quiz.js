module.exports = {
  attributes: {
      id: {columnName: 'quiz_code', type: 'string', required: true, unique: true, example: 'asd2ed', description: 'A 6 digit alphanumeric code for joining a quiz', maxLength: 6},
      qm_id: {type: 'string', required: true, example: '123456', description: 'A code linking to the creator of the quiz'},
      //id: {columnName:'q_id', type: 'string', required: true, example: '1233ds', description: 'A code to link to the question ID, can be many'},
      quiz_name: {type: 'string', example: 'WIPs', maxLength: 20},
  },
};