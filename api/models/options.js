module.exports = {
    attributes: {
        q_id: {
            type: 'string',
            required: false,
            example: '23dfd3'
        },

        quiz_code: {
          type: 'string',
          required: false,
          example: '23saerd'
        },

        id: {
            columnName: 'option_id',
            type: 'string',
            required: true,
            unique: true,
            example: '563dwd'
        },

        answer: {
            type: 'string',
            required: true,
            example : 'WIPs is work instructions for product support'
        },

        is_correct: {
            type: 'boolean',
            required: false,
            example: true
        }

    },
};