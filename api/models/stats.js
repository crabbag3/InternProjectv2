module.exports = {
    attributes: {
        id: {
           autoIncrement: true,
           columnName: 'test',
            type: 'number'
        },

        quiz_id: {
            type: 'string',
            required: false,
            example: '23dfd3'
        },

        question_id: {
            type: 'string',
            required: true,
            example: '22'
        },

        selected_option: {
            type: 'string',
            required: true,
            example : '33'
        },

        is_correct: {
            type: 'boolean',
            required: true,
            example: true
        },

        time: {
            type: 'string',
            required: false,
            example: '20'
        }

    },
};